module de.cwolff.uebungmehrwertsteuer {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens de.cwolff.uebungmehrwertsteuer to javafx.fxml;
    exports de.cwolff.uebungmehrwertsteuer;
}