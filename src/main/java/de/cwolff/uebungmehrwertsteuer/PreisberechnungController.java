package de.cwolff.uebungmehrwertsteuer;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class PreisberechnungController {
    @FXML //yay Data Binding!
    private TextField preisOhneMwStField;

    @FXML
    private TextField preisMitMwStField;

    private Preis preis;
    int mwSt = 19; //in Prozent

    @FXML
    protected void onZuruecksetzenButtonClick() {
        preisOhneMwStField.clear();
        preisMitMwStField.clear();
    }

    @FXML
    protected void onInputPreisOhneMwSt() {
        Double amount = Double.parseDouble(preisOhneMwStField.getText());
        Preis preis = new Preis(amount, mwSt);
        Double preisOhneMwSt = preis.getPreisMitMwSt();
        preisMitMwStField.setText(preisOhneMwSt.toString());
    }

    @FXML
    protected void onInputPreisMitMwSt() {
        Double amount = Double.parseDouble(preisMitMwStField.getText());
        Preis preis = new Preis(amount, mwSt);
        Double preisOhneMwSt = preis.getPreisOhneMwSt();
        preisOhneMwStField.setText(preisOhneMwSt.toString());
    }
}