package de.cwolff.uebungmehrwertsteuer;

public class Preis {

    private double amount;
    private int mehrwertsteuer; //in Prozent

    public Preis(double amount, int mehrwertsteuer) {
        this.amount = amount;
        this.mehrwertsteuer = mehrwertsteuer;
    }

    public double getPreisMitMwSt(){
        return amount + (amount * mehrwertsteuer/100);
    }

    public double getPreisOhneMwSt(){
        return amount + (amount * mehrwertsteuer/100);
    }

}
